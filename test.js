var co = require('co');
var dnet = require('./lib/main');


dnet.on('load', function () {
	console.log('load');
	console.log('test0 new message');
	var msg = new dnet.Message().toBuffer();
	new dnet.Message(msg);
	console.log('test0 passed');
	co(function* () {
		console.log('test1 sending');
		yield (new Promise((resolve, reject) => {
			dnet.connection.subscribe('test1', function(m) { 
				if (m.nodeID.equal(dnet.NodeID.myNodeID)) {
					console.log('test1 passed'); 
					resolve();
				}
			});
			msg = new dnet.Message();
			msg.type = 'test1';
			dnet.connection.send(msg, { address: '::1', port: dnet.environment.localPort });
			setTimeout(reject.bind(null, Error('test1 timeout')), 5000);
		}));
		console.log('test2 rpc');
		dnet.RPC.register('test', function (a, b) { return a + b; });
		var r = dnet.RPC.call({ address: '::1', port: dnet.environment.localPort }, 'test')(3, 5);
		console.log((yield r) === 8 ? 'test2 passed' : 'test2 failed');
		console.log('test3 find my address');
		if ((yield dnet.NodeID.myNodeID.address())[0].address === '::1') console.log('test3 passed');
	}).catch(err => console.log(err.stack));
});
