#!/bin/bash

make_one () {
	mkdir /tmp/$1
	cd /tmp/$1
	ln -s ~/Projects/decentralized-network/node_modules ./
	ln -s ~/Projects/decentralized-network/test-group.js ./
	ln -s ~/Projects/decentralized-network/test-main.js ./
	ln -s ~/Projects/decentralized-network/lib ./
	rm nodes.db
	node ./test-group
}

for ((i = 1; i <= 100; i++)); do
	make_one $i &
#	sleep 1;
done
