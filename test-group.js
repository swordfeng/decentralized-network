var co = require('co');
var repl = require('repl');
root.dnet = require('./lib/main');


dnet.on('load', co.wrap(function* () {
	console.log('load');
	yield dnet.ping({
		address: '::1',
		port: 52233
	});
	console.log(dnet.kad);
	yield dnet.kad.bootstrap();
}));
