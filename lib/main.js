/**
 * This file integrates all other modules.
 **/

root.co = require('co');
root.Promise = require('bluebird');
Promise.promisifyAll(require('nedb').prototype);
root.GeneratorFunction = (function* () {}).constructor;

var log = require('./log');
var events = require('events');

var dnet = module.exports = new events.EventEmitter;

var env = dnet.environment = require('./environment');
var NodeID = dnet.NodeID = require('./node-id');
var Message = dnet.Message = require('./message');
var connection = dnet.connection = require('./connection');
var RPC = dnet.RPC = require('./rpc');
var ping = dnet.ping = require('./ping');
var kad = dnet.kad = new (require('./kad'))('kad');

var upnp = require('./upnp');
var log = require('./log');


dnet.init = co.wrap(function* () {
    // load options
	yield env.init();
    // listen
    yield connection.init();
    // upnp
	try {
		yield upnp.upnpPortMap();
	} catch (err) {
		log.warn('upnp failed: ' + err.message);
	}
    // stun test
    // ping a node list
	// kad botstrap
});

dnet.init()
.then(function () {
	dnet.emit('load');
})
.catch(err => console.error(err.stack));
