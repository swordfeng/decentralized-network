var iptool = exports;

iptool.normalize = function(ip) {
	ip = iptool.formalize(ip);
	var res = /^::ffff:([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)$/.exec(ip);
	if (res !== null) ip = res[1] + '.' + res[2] + '.' + res[3] + '.' + res[4];
	return ip;
};

iptool.formalize = function (ip) {
	var net = require('net');
	if (!net.isIP(ip)) throw Error('wrong ip format');
	ip = ip.toLowerCase();
	if (net.isIPv4(ip)) ip = '::ffff:' + ip;
	var v4res = /^[0-9a-fA-F:]*:([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)$/.exec(ip);
	if (v4res !== null) {
		ip = '::ffff:' + (parseInt(v4res[1])*256+parseInt(v4res[2])).toString(16) + ':' + (parseInt(v4res[3])*256+parseInt(v4res[4])).toString(16);
	}
	var v6res = /^.*::.*$/.exec(ip);
	if (v6res !== null) {
		var count = 0;
		for (var i = 0; i < ip.length; i++) if (ip[i] === ':') count++;
		var repstr = ':0:';
		for (var i = 0; i < 7 - count; i++) repstr += '0:';
		ip = ip.replace(/::/, repstr);
		if (ip[0] === ':') ip = '0' + ip;
		if (ip[ip.length - 1] === ':') ip = ip + '0';
	}
	var v6segs = ip.split(':');
	ip = parseInt(v6segs[0], 16).toString(16);
	for (var i = 1; i < 8; i++) ip += ':' + parseInt(v6segs[i], 16).toString(16);
	ip = ip.replace(/(0:)*0/, '');
	if (ip[0] === ':') ip = ':' + ip;
	if (ip[ip.length - 1] === ':') ip = ip + ':';
	var conv4 = /::ffff:([0-9a-f]+):([0-9a-f]+)/.exec(ip);
	if (conv4 !== null) {
		var ab = parseInt(conv4[1], 16);
		var cd = parseInt(conv4[2], 16);
		var a = ab >> 8;
		var b = ab & 0xff;
		var c = cd >> 8;
		var d = cd & 0xff;
		ip = '::ffff:' + a + '.' + b + '.' + c + '.' + d;
	}
	if (!net.isIPv6(ip)) throw ('unexpected error');
	return ip;
};

