/**
 * This module implements a simple RPC protocol.
 **/

var RPC = module.exports;

var co = require('co');

var connection = require('./connection');
var Message = require('./message');
var log = require('./log');

const RPC_TIMEOUT = 10000;

var pendingMap = {};

RPC.register = function (methodName, func) {
	connection.subscribe('rpc:call:' + methodName, function (recvMsg, address) {
		log.info('RPC: (callee) call ' + methodName + ' from ' + JSON.stringify(address));
		function succ(result) {
			log.info('RPC: (callee) return ' + methodName+ ' to ' + JSON.stringify(address));
			var sendMsg = new Message;
			sendMsg.type = 'rpc:return:' + methodName;
			sendMsg.payload = {
				uuid: recvMsg.uuid,
				result: result
			};
			connection.send(sendMsg, address).catch(err => log.debug(err));
		}
		function fail(err) {
			log.info('RPC: (callee) throw ' + methodName + ' to ' + JSON.stringify(address));
			log.info('RPC ' + err.stack)
			var sendMsg = new Message;
			sendMsg.type = 'rpc:throw:' + methodName;
			sendMsg.payload = {
				uuid: recvMsg.uuid,
				name: err.name,
				message: err.message
			};
			connection.send(sendMsg, address).catch(err => log.debug(err));
		}
		try {
			if (func.__proto__ == (function* () {}).__proto__) {
				var result = co.wrap(func).apply({
					nodeID: recvMsg.nodeID,
					address: address
				}, recvMsg.payload);
			} else {
				var result = func.apply({
					nodeID: recvMsg.nodeID,
					address: address
				}, recvMsg.payload);
			}
		} catch (err) {
			fail(err);
			return;
		}
		if (result instanceof Promise) {
			result.then(succ, fail);
		} else {
			succ(result);
		}
	});
	connection.subscribe('rpc:return:' + methodName, function (message, address) {
		if (pendingMap[message.payload.uuid]) pendingMap[message.payload.uuid].resolve(message.payload.result);
	});
	connection.subscribe('rpc:throw:' + methodName, function (message, address) {
		var err = new Error(message.payload.message);
		err.name = message.payload.name;
		if (pendingMap[message.payload.uuid]) pendingMap[message.payload.uuid].reject(err);
	});
};

RPC.call = function (address, methodName) {
	return function () {
		log.info('RPC: (caller) call ' + methodName + ' to ' + JSON.stringify(address));
		var args = Array.prototype.slice.apply(arguments);
		var uuid = null;
		return new Promise(function (resolve, reject) {
			var sendMsg = new Message;
			uuid = sendMsg.uuid;
			pendingMap[sendMsg.uuid] = {
				reject: reject,
				resolve: resolve
			};
			sendMsg.type = 'rpc:call:' + methodName;
			sendMsg.payload = args;
			connection.send(sendMsg, address).catch(reject);
			setTimeout(() => {
				if (pendingMap[sendMsg.uuid]) {
					var err = new Error('RPC timeout');
					err.type = 'RPC:TIMEOUT';
					reject(err);
				}
			}, RPC_TIMEOUT);
		}).then(
			function (res) {
				log.info('RPC: (caller) return ' + methodName + ' from ' + JSON.stringify(address));
				delete pendingMap[uuid];
				return res;
			},
			function (err) {
				log.info('RPC: (caller) throw ' + methodName + ' ' + err.message + ' from ' + JSON.stringify(address));
				delete pendingMap[uuid];
				throw err;
			}
		);
	};
};
