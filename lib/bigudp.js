var util = require('util');
var events = require('events');
var dgram = require('dgram');
var crypto = require('crypto');

function bigdgram(socket){
	this._socket = socket;
	this._packet_list = {};
	var self = this;
	socket.on("message", function(buf, rinfo) {
		var index = rinfo.address + rinfo.port + buf.slice(2, 10).toString('hex');
		if (buf[0] == 0xff) { //ack
			index = 'out' + index;
			if (self._packet_list[index]) {
				if (self._packet_list[index].ack<buf[1]) self._packet_list[index].ack = buf[1];
				if (self._packet_list[index].ack == self._packet_list[index].len) {
					self._packet_list[index].cb(null);
					clearInterval(self._packet_list[index].refresh);
					delete(self._packet_list[index]);
				}
			} //else socket.send(Buffer.concat([new Buffer([0xfe, 0x00]), buf.slice(2, 10)]), 0, 9, rinfo.port, rinfo.address);
		} else {
			index = 'in' + index;
			/*
			if (buf[0] === 0xfe && buf[1] === 0x00) {
				// end ack
				if (self._packet_list[index]) {
					clearTimeout(self._packet_list[index].refresh);
					delete(self._packet_list[index]);
				}
				return;
			}
			* */
			if (!self._packet_list[index]) {
				self._packet_list[index] = new packet();
				self._packet_list[index].refresh = setTimeout(function() {
					delete(self._packet_list[index]);
				}, 5000);
			}
			self._packet_list[index].update(buf);
			var ackpack = new Buffer(10);
			ackpack[0] = 0xff;
			ackpack[1] = self._packet_list[index].recv;
			buf.copy(ackpack, 2, 2, 10);
			self._socket.send(ackpack,0,ackpack.length,rinfo.port,rinfo.address);
			if (self._packet_list[index].finish()) {
				self.emit("message", self._packet_list[index].getbuf(), rinfo);
				clearTimeout(self._packet_list[index].refresh);
				self._packet_list[index].refresh = setTimeout(function() {
					delete(self._packet_list[index]);
				}, 15000);
			}
		}
	});
};

util.inherits(bigdgram, events.EventEmitter);

bigdgram.prototype.send = function(buf, saddr, callback) {
	var pack = new packet(buf);
	pack.cb = callback;
	var index = 'out' + saddr.address + saddr.port + pack.cookie.toString('hex');
	this._packet_list[index] = pack;
	var self = this;
	var sendcount = 0;
	var sendfunc = function() {
		for (var i=pack.ack;i<pack.len;i++) {
			self._socket.send(pack.getslice(i),0,pack.getslice(i).length,saddr.port,saddr.address,function(err) {
				if (err) {
					pack.cb(err);
					if (self._packet_list[index].refresh) {
						clearInterval(self._packet_list[index].refresh);
					}
					delete(self._packet_list[index]);
				}
			});
		}
		if (sendcount++ == 10) {
			pack.cb(new Error("send time out"));
			clearInterval(self._packet_list[index].refresh);
			delete(self._packet_list[index]);
		}
	};
	this._packet_list[index].refresh = setInterval(sendfunc, 500);
	sendfunc();
};

function packet(buf) {
	if (buf) {
		this.len = parseInt(((buf.length||1)-1)/512)+1;
		this.ack = 0;
		this.cookie = crypto.randomBytes(8);
		for (var i=0;i<this.len;i++) {
			this[i] = buf.slice(512*i, 512*(i+1));
		}
	} else {
		this.recv = 0;
		this.hadfinish = false;
	}
}

//packet.prototype.ack = 0;

packet.prototype.update = function(buf) {
	this.len = buf[1];
	this.cookie = buf.slice(2,10);
	this[buf[0]] = buf.slice(10);
	while (this[this.recv]) this.recv++;
};

packet.prototype.getbuf = function() {
	var llen = 0;
	if (!this.len) return;
	for (var i=0;i<this.len;i++) llen+=this[i].length;
	var bbuf = new Buffer(llen);
	for (var i=0;i<this.len;i++) this[i].copy(bbuf,512*i);
	return bbuf;
}
	
packet.prototype.getslice = function(i) {
	var buf = new Buffer(this[i].length+10);
	buf[0]=i;
	buf[1]=this.len;
	this.cookie.copy(buf,2);
	this[i].copy(buf,10);
	return buf;
};

packet.prototype.finish = function() {
	if (this.hadfinish) return false;
	return (this.hadfinish = (this.len == this.recv));
}


var protocol = {};
var server = null;
var bdserver = null;
var handleMessage = null;

protocol.init = function* (port, handleMessage) {
	server = dgram.createSocket('udp6');
	server.bind(port);
	bdserver = new bigdgram(server);
	bdserver.on('message', (buf, rinfo) => handleMessage(buf, {
		address: rinfo.address,
		port: rinfo.port
	}));
};

protocol.send = function (buf, address) {
	return new Promise((resolve, reject) => {
		bdserver.send(buf, address, err => {
			if (err === null) resolve();
			else reject(err)
		});
	});
};


module.exports = protocol;
