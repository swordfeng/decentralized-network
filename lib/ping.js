var util = require('util');

var RPC = require('./rpc');
var log = require('./log');
var NodeID = require('./node-id');

var ping = co.wrap(function* (node) {
	if (node instanceof NodeID) {
		var nodeID = node;
		log.info('ping ' + nodeID.toString('hex'));
		var addresses = yield nodeID.address();
		if (addresses.length === 0) {
			var err = new Error('No address binding for node ' + nodeID.toString());
			err.type = 'PING:NODE_NOT_FOUND';
			throw err;
		} else {
			var address = addresses[0];
			var error = null;
			if (address.v4) {
				try {
					return yield RPC.call(address.v4, 'ping')();
				} catch (err) {
					if (err.type === 'RPC:TIMEOUT') err.type = 'PING:TIMEOUT';
					error = err;
				}
			}
			if (address.v6) {
				try {
					return yield RPC.call(address.v6, 'ping')();
				} catch (err) {
					if (err.type === 'RPC:TIMEOUT') err.type = 'PING:TIMEOUT';
					error = err;
				}
			}
			if (error) throw error;
		}
	} else {
		var address = node;
		log.info('ping ' + util.inspect(address));
		try {
			return yield RPC.call(address, 'ping')();
		} catch (err) {
			if (err.type === 'RPC:TIMEOUT') err.type = 'PING:TIMEOUT';
			throw err;
		}
	}
});

RPC.register('ping', function () {
	return true;
});

module.exports = ping;
