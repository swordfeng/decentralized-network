/**
 * This module provides Kademlia operations.
 **/
var assert = require('assert');
var co = require('co');
var _ = require('underscore');
var util = require('util');
var net = require('net');

var log = require('./log');
var NodeID = require('./node-id');
var RPC = require('./rpc');
var env = require('./environment');
var connection = require('./connection');
var iptool = require('./ip');

const MAX_BUCKET_SIZE = 32;
const FIND_NODE_PARALLEL = 3;
const FIND_NODE_LIST_SIZE = 32;

/** definition of KBucket **/
function KBucket(parent, nodeID, prefixLength) {
	if (!(parent instanceof KBucket)) {
		this.nodeID = new NodeID();
		this.prefixLength = 0;
		this.KAD = parent;
	} else {
		this.nodeID = nodeID;
		this.prefixLength = prefixLength;
		this.parent = parent;
		this.KAD = parent.KAD;
	}
	this.nodeList = [];
}

KBucket.prototype = {
	nodeID: null,
	prefixLength: 0,
	left: null,
	right: null,
	parent: null,
	nodeList: null,
	mainLine: true,
	KAD: null,
	get pair () {
		if (this.parent === null) return null;
		var significantBit = this.nodeID.bit(this.prefixLength - 1);
		if (significantBit === 1) return this.parent.left;
		else return this.parent.right;
	}
};

KBucket.prototype.split = function () {
	if (this.nodeList === null) return;
	if (!this.isFull()) return;
	log.info('KBUCKET: split bucket ' + this.nodeID.toString('hex') + '/' + this.prefixLength);
	var leftID = this.nodeID.clone();
	var rightID = this.nodeID.clone();
	leftID.bit(this.prefixLength, 0);
	rightID.bit(this.prefixLength, 1);
	this.left = new KBucket(this, leftID, this.prefixLength + 1);
	this.right = new KBucket(this, rightID, this.prefixLength + 1);
	this.nodeList.map(nodeID => {
		if (nodeID.bit(this.prefixLength) === 0) this.left.insert(nodeID);
		else this.right.insert(nodeID);
	});
	this.nodeList = null;
};

KBucket.prototype.insert = co.wrap(function* (nodeID) {
	if (!this.parent) log.info('KBUCKET: insert ' + nodeID.toString());
	if (this.nodeList === null) {
		if (nodeID.bit(this.prefixLength) === 0) this.left.insert(nodeID);
		else this.right.insert(nodeID);
	} else if (_.any(this.nodeList, id => id.equal(nodeID))) {
		this.nodeList = _.chain(this.nodeList).filter(id => !id.equal(nodeID)).union([nodeID]).value();
	} else { 
		if (this.isFull()) {
			// judge whether this bucket shoule be splitted or kept
			if (this.mainLine) {
				this.nodeList.push(nodeID);
				this.split();
				if (this.contain(NodeID.myNodeID) && this.pair) this.pair.unsetMainLine();
			} else {
				// check the first one in list
				while (this.isFull()) {
					if (yield this.KAD.isActive(this.nodeList[0])) break;
				}
				if (!this.isFull()) this.insert(nodeID);
			}
		} else {
			this.nodeList.push(nodeID);
		}
	}
});

KBucket.prototype.remove = function (nodeID) {
	if (!this.parent) log.info('KBUCKET: remove ' + nodeID.toString());
	if (this.nodeList === null) {
		if (this.left.contain(nodeID)) this.left.remove(nodeID);
		else this.right.remove(nodeID);
	} else {
		this.nodeList = _.filter(this.nodeList, ID => !ID.equal(nodeID));
		if (this.mainLine && !this.isFull() && this.pair) this.pair.setMainLine();
	}
};

KBucket.prototype.isFull = function () {
	if (this.nodeList === null || this.nodeList.length >= MAX_BUCKET_SIZE) return true;
	else return false;
};

KBucket.prototype.contain = function (nodeID) {
	nodeID = nodeID.clone();
	for (var i = this.prefixLength; i < NodeID.NODE_ID_LENGTH * 8; i++) nodeID.bit(i, 0);
	return this.nodeID.equal(nodeID);
};

KBucket.prototype.countBucket = function () {
	if (this.nodeList === null) {
		return this.left.countBucket() + this.right.countBucket() + 1;
	} else return 1;
};

KBucket.prototype.countNode = function () {
	if (this.nodeList === null) {
		return this.left.countNode() + this.right.countNode();
	} else return this.nodeList.length;
};

KBucket.prototype.setMainLine = function () {
	if (this.mainLine) return;
	this.mainLine = true;
	if (this.nodeList === null) {
		var significantBit = NodeID.myNodeID.bit(this.prefixLength);
		if (significantBit === 0) {
			this.left.setMainLine();
			if (!this.left.isFull()) this.right.setMainLine();
		} else {
			this.right.setMainLine();
			if (!this.right.isFull()) this.left.setMainLine();
		}
	}
};

KBucket.prototype.unsetMainLine = function () {
	if (!this.mainLine) return;
	this.mainLine = false;
	if (this.nodeList === null) {
		this.left.unsetMainLine();
		this.right.unsetMainLine();
		this.nodeList = _.union(this.left.nodeList, this.right.nodeList);
		this.left = null;
		this.right = null;
	}
};

KBucket.prototype.findNode = function (nodeID) {
	if (this.nodeList === null) {
		var sigBit = nodeID.bit(this.prefixLength);
		var result = [];
		if (sigBit === 0) {
			result = this.left.findNode(nodeID);
			if (result.length < FIND_NODE_LIST_SIZE) result = result.concat(this.right.findNode(nodeID));
		} else {
			result = this.right.findNode(nodeID);
			if (result.length < FIND_NODE_LIST_SIZE) result = result.concat(this.left.findNode(nodeID));
		}
	} else {
		var result = this.nodeList.slice(0);
	}
	if (result.length > FIND_NODE_LIST_SIZE) {
		result.sort((nodeID1, nodeID2) => nodeID.distance(nodeID1).compare(nodeID.distance(nodeID2)));
		result = result.slice(0, FIND_NODE_LIST_SIZE);
	}
	return result;
};

/** definition of kad operations **/
function KAD(name, msgFilter) {
	assert(msgFilter === undefined || typeof msgFilter === 'function');
	this.name = name;
	this.rootBucket = new KBucket(this);
	this.rootBucket6 = new KBucket(this);

	var self = this;

	RPC.register(this.name + ':ping', () => true);

	RPC.register(this.name + ':findNode', function* (idStr, ipversion) {
		var nodeID = new NodeID(idStr);
		var rootBucket = self.rootBucket;
		if (ipversion && ipversion === 6) rootBucket = self.rootBucket6;
		var nodeList = rootBucket.findNode(nodeID);
		for (var i = 0; i < nodeList.length; i++) {
			var nodeID = nodeList[i];
			var address = yield nodeID.address();
			if (address.length !== 0) {
				nodeList[i] = [{
					nodeID: nodeID.toString(),
					address: address[0]
				}];
			} else nodeList[i] = [];
		}
		return _.flatten(nodeList);
	});

	env.userAgent[this.name] = '0.0.1';
	env.on('nodeID:remove', nodeID => this.rootBucket.remove(nodeID));

	connection.subscribe('*', function (msg, address) {
		if (!msg.userAgent[self.name]) return;
		if (msgFilter && !msgFilter(msg, address)) return;
		// check kad version
		// check publicity
		if (net.isIPv4(iptool.normalize(address.address))) {
			self.rootBucket.insert(msg.nodeID);
		} else {
			self.rootBucket6.insert(msg.nodeID);
		}
	});
}

KAD.prototype = {
	rootBucket: null,
	rootBucket6: null,
	name: 'kad'
};

KAD.prototype.ping = co.wrap(function* (nodeID, ipversion) {
	log.info(this.name + ': ping ' + nodeID.toString());
	var addresses = yield nodeID.address();
	var rootBucket = ipversion === 4 ? this.rootBucket : this.rootBucket6;
	if (addresses.length === 0 || !addresses[0]['v' + ipversion]) {
		var err = new Error('No ipv' + ipversion + ' address binding for node ' + nodeID.toString());
		err.type = 'KAD:PING:NODE_NOT_FOUND';
		rootBucket.remove(nodeID);
		throw err;
	} else {
		var address = addresses[0]['v' + ipversion];
		try {
			return yield RPC.call(address, this.name + ':ping')();
		} catch (err) {
			if (err.type === 'RPC:TIMEOUT') rootBucket.remove(nodeID);
			throw err;
		}
	}
});

KAD.prototype.findNode = co.wrap(function* (nodeID, ipversion) {
	assert(nodeID instanceof NodeID);
	log.info(this.name + ': find ' + nodeID.toString());
	function nearest(nodeA, nodeB) {
		return nodeID.distance(nodeA.nodeID).compare(nodeID.distance(nodeB.nodeID));
	}
	
	if (!ipversion) ipversion = 4;
	var rootBucket = ipversion === 4 ? this.rootBucket : this.rootBucket6;

	var nodeList = rootBucket.findNode(nodeID);
	for (var i = 0; i < nodeList.length; i++) {
		var address = yield nodeList[i].address();
		if (address.length > 0 && address[0]['v' + ipversion]) {
			nodeList[i] = [{
				nodeID: nodeList[i],
				address: address[0],
				asked: false
			}];
		} else {
			nodeList[i] = [];
		}
	}
	nodeList = _.flatten(nodeList);
	
	while (_.any(nodeList, node => node.asked == false)) {
		var i;
		for (i = 0; i < nodeList.length; i++) if (!nodeList[i].asked) break;
		nodeList[i].asked = true;
		if (nodeList[i].nodeID.equal(NodeID.myNodeID)) continue;
		try {
			var result = yield RPC.call(nodeList[i].address['v' + ipversion], this.name + ':findNode')(nodeID.toString(), ipversion);
		} catch (err) {
			console.log(err.stack);
			var result = [];
		}
		log.debug(this.name + ': got ' + result.length +' nodes');
		nodeList = nodeList.concat(result.map(nodeInfo => {
			nodeInfo.nodeID = new NodeID(nodeInfo.nodeID);
			nodeInfo.asked = false;
			return nodeInfo;
		})).sort(nearest);
		
		i = 1;
		while (i < nodeList.length) {
			if (nodeList[i].nodeID.equal(nodeList[i-1].nodeID)) {
				if (nodeList[i].asked) nodeList[i-1].asked = true;
				nodeList.splice(i, 1);
			} else i++;
		}
		nodeList = nodeList.slice(0, FIND_NODE_LIST_SIZE);
	}
	var res = nodeList.map(node => node.nodeID);
	log.info(this.name + ': found nodes ' + util.inspect(res.map(nodeID => nodeID.toString())));
});


KAD.prototype.isActive = co.wrap(function* (nodeID) {
	var lastActive = yield nodeID.lastActive();
	if (lastActive !== null && new Date() - lastActive < 60000) return true;
	try {
		yield this.ping(nodeID);
		return true;
	} catch (err) {
		if (err.type === 'KAD:PING:NODE_NOT_FOUND' || err.type === 'RPC:TIMEOUT') return false;
		else throw err;
	}
});

KAD.prototype.bootstrap = co.wrap(function* () {
	var nodeCount = this.rootBucket.countNode() + this.rootBucket6.countNode();
	if (nodeCount === 0) {
		// get some nodes
		var err = new Error('KAD: fail to bootstrap');
		err.type = 'KAD:BOOTSTRAP_FAIL';
		throw err;
	}
	yield this.findNode(NodeID.myNodeID, 4);
	yield this.findNode(NodeID.myNodeID, 6);
});

module.exports = KAD;
