/**
 * This module provides global envirionment variables for the networking part.
 **/
var assert = require('assert');
var events = require('events');
var Datastore = require('nedb');
var log = require('./log');

var Err = require('./err');

var configDB = new Datastore({
	filename: 'config.db',
	autoload: true
});
configDB.persistence.compactDatafile();

var env = module.exports = new events.EventEmitter;

env.userAgent = {
	protocol: ['tcp']
};
env.protocol = ['tcp'];
env.ipv4 = true;
env.ipv6 = true;

// config first set
env.init = co.wrap(function* () {
	var log = require('./log');
	log.info('ENV: init');
	var inited = yield env.config('initialized');
	if (inited === undefined || inited === false) {
		yield initialize();
	}
	env.localPort = yield env.config('localPort');
	env.keyType = yield env.config('keyType');
	env.publicKey = new Buffer(yield env.config('publicKey'), 'base64');
	env.privateKey = new Buffer(yield env.config('privateKey'), 'base64');
	env.emit('env:init');
	var util = require('util');
	log.info('ENV: entities: ' + util.inspect(env));
	log.success('ENV: inited');
});

env.config = co.wrap(function* (key, value) {
	assert(typeof key === 'string');
	if (value === undefined) {
		// get option
		var res = yield configDB.findAsync({ key: key });
		if (res.length === 0) return undefined;
		else return res[0].value;
	} else if (value === null) {
		// delete option
		yield configDB.removeAsync({ key: key });
		env.emit('config:' + key, null);
	} else {
		// set option
		yield configDB.update({ key: key }, {
			key: key,
			value: value
		}, { upsert: true });
		env.emit('config:' + key, value);
	}
});

function* initialize() {
	log.info('ENV: First time initialize');
	var crypto = require('crypto');
	var sodium = require('sodium');
	var keys = sodium.Key.Sign.fromSeed(crypto.randomBytes(32));
	var initialObj = {
		initialized: true,
		localPort: parseInt(Math.random()*(65536 - 2048) + 2048),
		keyType: 'ed25519',
		publicKey: keys.publicKey.toString(),
		privateKey: keys.secretKey.toString()
	};
	var insertArray = [];
	for (var key in initialObj) {
		log.info('\tInitialize: \t' + key + ' = ' + initialObj[key]);
		insertArray.push({
			key: key,
			value: initialObj[key]
		});
	}
	var result = yield configDB.insertAsync(insertArray);
	if (result.length > 0) log.success('ENV: Initialize: ' + result.length + ' items successfully written');
	else throw Error('Initialize failed');
}
