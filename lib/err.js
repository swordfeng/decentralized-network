var util = require('util');
var Err = module.exports = function Err(type, message) {
	if (this === root) {
		var err = new Error(message);
		err.name = 'DNet Error';
		err.type = type;
		return err;
	} else {
		Error.call(this, message);
		this.name = 'DNet Error';
		this.type = type;
	}
};

util.inherits(Err, Error);
