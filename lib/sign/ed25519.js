var sodium = require('sodium');

var ed25519 = module.exports = {};

ed25519.sign = function(buffer, privateKey) {
	var signer = new sodium.Sign(new sodium.Key.Sign(new Buffer(32), privateKey));
	var result = signer.signDetached(buffer);
	return result.sign;
}

ed25519.verify = function(buffer, signature, publicKey) {
	return sodium.Sign.verifyDetached({
		sign: signature,
		publicKey: publicKey
	}, buffer);
}
