var Promise = require('bluebird');
var co = require('co');
var env = require('./environment');
var natUpnp = require('nat-upnp');

var log = require('./log');

function upnpPortMap() {
	return co(function* () {
		log.info('startting upnp...');
		log.info('UPNP - Port = ' + env.localPort);
		var client = Promise.promisifyAll(natUpnp.createClient());
		yield (client.portMappingAsync({
			public: env.localPort,
			private: env.localPort,
			ttl: 1000,
			protocol: 'udp',
			description: 'dnet:' + env.localPort
		}));
		var mappingList = yield (client.getMappingsAsync({local:true}));
		log.info('upnp started, got mapped list:');
		log.info(mappingList);
	});
}

exports.upnpPortMap =  upnpPortMap;
