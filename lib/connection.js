/**
 * This is the base connecting module.
 **/

// this module is currently implemented by node-rudp;
// the protocol (I think) does not implement any congestion control methods.

var connection = module.exports;

var assert = require('assert');

var log = require('./log');
var Err = require('./err');
var iptool = require('./ip');
var env = require('./environment');
var Message = require('./message');

var protocol = require('./bigudp');
var messageListeners = {};

connection.send = co.wrap(function* (message, address) {
	assert(message instanceof Message, 'invalid message');
	assert(address, 'invalid address');

	address.address = iptool.formalize(address.address);

	var msgBuf = message.toBuffer();
	try {
		yield protocol.send(msgBuf, address);
	} catch (err) {
		log.debug(err.stack);
		throw Err('CONN:FAIL_TO_SEND', 'fail to send the message');
	}
	log.info('CONN: Send message length: ' + msgBuf.length);
});

/*
 * callback is function(message, address)
 * messageType === '*' means all messages
 * messageType is recommended in format * | module:* | module:name
 * for further dispatching
 */
connection.subscribe = function (messageType, callback) {
	assert(typeof messageType === 'string', 'invalid message type');
	assert(typeof callback === 'function', 'invalid callback');
	var indexName = 'msg_' + messageType;
	if (messageListeners[indexName] === undefined) {
		messageListeners[indexName] = [];
	}
	messageListeners[indexName].push(callback);
	log.info('CONN: register callback for type:' + messageType);
};

var handleRegex = /([^:]*):(.*)/;
function handleMessage(msgBuf, address) {
	assert(address);
	log.info('CONN: Recv messsage length: ' + msgBuf.length);
	try {
		var msg = new Message(msgBuf);
	} catch (err) {
		if (err.type === 'MSG:MALFORMED_MESSAGE') return;
		else throw err;
	}
	try {
		msg.nodeID.address(address);
		msg.nodeID.activate();
	} catch (err) {
		console.error(err.stack);
	}
	
	var cbList = [];
	var indexName = 'msg_' + msg.type;
	if (messageListeners[indexName]) cbList = cbList.concat(messageListeners[indexName]);
	var match = handleRegex.exec(msg.type);
	if (match !== null) {
		var indexName = 'msg_' + match[1] + ':*';
		if (messageListeners[indexName]) cbList = cbList.concat(messageListeners[indexName]);
	}
	if (messageListeners['msg_*']) cbList = cbList.concat(messageListeners['msg_*']);
	
	for (var cb of cbList) try {
		cb(msg, address);
	} catch (err) { console.error(err.stack); }
}

connection.init = co.wrap(function* () {
	yield protocol.init(env.localPort, handleMessage);
});


