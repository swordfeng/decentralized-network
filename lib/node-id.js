/**
 * This module defines NodeID formats, its object structure, 
 * and implements related methods.
 **/
var assert = require('assert');
var co = require('co');
var crypto = require('crypto');
var Datastore = require('nedb');
var env = require('./environment');
var log = require('./log');
var iptool = require('./ip');
var net = require('net');

var db = new Datastore({
	filename: 'nodes.db',
	autoload: true
});
db.persistence.setAutocompactionInterval(900000); // auto compact every 15min

const NODE_ID_LENGTH = 32;

// create ID object
var NodeID = module.exports = function (ID) {
	if (!this instanceof NodeID) {
		return NodeID.apply(Object.create(NodeID.prototype), arguments);
	}
	if (ID !== undefined) {
		this._id = new Buffer(ID, 'base64');
		if (this._id.length !== NODE_ID_LENGTH && ID.length === 64) this._id = new Buffer(ID, 'hex');
		if (this._id.length !== NODE_ID_LENGTH) throw Error('invalid id');
	} else {
		this._id = new Buffer(NODE_ID_LENGTH).fill(0);
	}
};

// ID structue
NodeID.prototype = {
	_id: null
};

NodeID.prototype.bit = function (position, value) {
	if (position >= NODE_ID_LENGTH * 8) {
		var err = new Error('NodeID bit index out of range');
		err.type = 'NODEID:OUT_OF_RANGE';
		throw err;
	}
	if (value === undefined) return (this._id[parseInt(position / 8)] >> (7 - position % 8)) & 1;
	else {
		assert(value === 0 || value === 1);
		var sigIndex = parseInt(position / 8);
		var sigBit = 7 - position % 8;
		var sigByte = this._id[sigIndex];
		sigByte = (sigByte & (~1 << sigBit)) | (value & 1) << sigBit;
		this._id[sigIndex] = sigByte;
	}
};
NodeID.prototype.equal = function (nodeID) { assert(nodeID instanceof NodeID); return this._id.equals(nodeID._id); };
NodeID.prototype.clone = function () {return new NodeID(this.toString());};
NodeID.prototype.toString = function (encode) {return this._id.toString(encode || 'base64');};
NodeID.prototype.toJSON = function () {return this.toString();};

NodeID.prototype.distance = function (nodeID) {
	assert(nodeID instanceof NodeID);
	var res = new Buffer(NODE_ID_LENGTH);
	for (var i = 0; i < NODE_ID_LENGTH; i++) res[i] = this._id[i] ^ nodeID._id[i];
	return res;
};
	
// get/set address
NodeID.prototype.address = co.wrap(function* (address) {
	if (address === undefined) {
		// get address
		return (yield db.findAsync({ nodeID: this.toString() })).map(pair => pair.address);
	} else if (typeof address === 'object') {
		assert(typeof address.address === 'string');
		assert(typeof address.port === 'number');
		address.address = iptool.normalize(address.address);
		//set address
		log.info('set address for node: ' + this.toString());
		log.info('address is: ' + JSON.stringify(address));
		var ipversion = net.isIP(address.address);
		var setObj = {
			nodeID: this.toString()
		};
		if (ipversion === 6) setObj["address.v6"] = address;
		else setObj["address.v4"] = address;
		yield (db.updateAsync({ nodeID: this.toString() }, { $set: setObj }, { upsert: true }));
	} else if (address === false) {
		log.info('remove address for node: ' + this.toString());
		env.emit('nodeID:remove', this);
		yield (db.removeAsync({ nodeID: this.toString() }, {}));
	}
});

NodeID.prototype.activate = co.wrap(function* () {
	yield db.updateAsync({ nodeID: this.toString() }, {
		$set: {
			lastActive: new Date()
		}
	}, {});
});

NodeID.prototype.lastActive = co.wrap(function* () {
	var queryRes = yield db.findAsync({ nodeID: this.toString() });
	if (queryRes.length === 0) return null;
	if (!queryRes[0].lastActive) return null;
	return queryRes[0].lastActive;
});

NodeID.NODE_ID_LENGTH = NODE_ID_LENGTH;

// NodeID.fromAddress = function (address) {};

NodeID.fromKey = function (publicKey) { assert(publicKey instanceof Buffer); return new NodeID(publicKey); };

NodeID.equal = function (nodeID1, nodeID2) { return nodeID1.equal(nodeID2); };
NodeID.distance = function (nodeID1, nodeID2) { return nodeID1.distance(nodeID2); };

env.on('env:init', () => NodeID.myNodeID = env.myNodeID = NodeID.fromKey(env.publicKey));
