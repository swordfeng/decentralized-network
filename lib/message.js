/**
 * This module defines message formats, message object structure, 
 * and implements related methods.
 **/
var assert = require('assert');
var crypto = require('crypto');

var log = require('./log');
var Err = require('./err');
var env = require('./environment');
var NodeID = require('./node-id');

var ed25519 = require('./sign/ed25519');

// create an empty message or create a message from buffer
var Message = module.exports = function (buffer) { // throws
    if (buffer === undefined) {
        // empty message
        this.uuid = crypto.randomBytes(32).toString('hex');
        this.date = new Date();
        this.nodeID = env.myNodeID;
		this.userAgent = env.userAgent;
    } else {
		assert(buffer instanceof Buffer);
		try {
			var msgObj = JSON.parse(buffer);
			if (!validate(msgObj)) throw Err('MSG:MALFORMED_MESSAGE', 'invalid message');
			for (var i of ['uuid', 'keyType',
					'userAgent', 'type', 'payload']) {
				this[i] = msgObj[i];
			}
			this.date = new Date(msgObj.date);
			this.nodeID = new NodeID(msgObj.nodeID);
			this.key = new Buffer(msgObj.key, 'base64');
		} catch (err) {
			log.warn('Invalid message!');
			throw err;
		}
    }
};

// message structure
Message.prototype = {
    uuid: null,
    nodeID: null,
    key: null,
	keyType: null,
    type: null,
    payload: null,
    userAgent: null,
    date: null
};

// serialization
Message.prototype.toBuffer = function () {
    var msgObj = {
        uuid: this.uuid,
        nodeID: this.nodeID.toString('base64'),
        keyType: '',
        key: null,
        keyExtra: null,
        signature: '',
        userAgent: this.userAgent,
        date: this.date.getTime(),
        type: this.type,
        payload: this.payload
    };
    
    sign(msgObj);
    
    var msgBuf = new Buffer(JSON.stringify(msgObj), 'utf8');
    return msgBuf;
};

function sign(obj) {
	obj.keyType = env.keyType;
    obj.key = env.publicKey.toString('base64');
    obj.signature = '';
    var buf = new Buffer(JSON.stringify(obj), 'utf8');
    obj.signature = ed25519.sign(buf, env.privateKey).toString('base64');
}

function validate(obj) {
	if (obj.keyType !== 'ed25519') throw Error('unknown sign algorithm');
	var signature = new Buffer(obj.signature, 'base64');
	obj.signature = '';
	var buf = new Buffer(JSON.stringify(obj), 'utf8');
	var ok = ed25519.verify(buf, signature, new Buffer(obj.key, 'base64'));
	return ok;
}

/*
function checkformat(obj) {
	if (typeof obj === 'function') return false;
	if (typeof obj === 'number' || typeof obj === 'string') return true;
	if (typeof obj.toJSON !== 'undefined') return false;
	if (typeof obj === 'object') {
		var res = true;
		for (var index in obj) {
			res = res && checkformat(index);
*/
